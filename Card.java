public class Card{
	// initializing fields
	private String suit;
	private String value;
	
	public Card(String suit, String value){ //constructor
		this.suit = suit;
		this.value = value;
	}
	//getters
	public String getSuit(){
		return this.suit;
	}
	public String getValue(){
		return this.value;
	}
	//toString method
	public String toString(){
		return this.value + " of " + this.suit;
	}
	
	public double calculateScore(){ //calculates the score of the drawn cards 
		String[] suits = new String[]{"Spades", "Clubs", "Diamonds", "Hearts"};
		String[] values = new String[]{"Ace", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King"};
		
		double valueScore = 0.0; //all values start at 0.0
		double suitScore = 0.0;
		double counter = 1.0; //The value of a suit must be a decimal, the counter allows us to print a decimal by dividing it by ten
		for(int i =0; i < values.length; i++){
			if(this.value.equals(values[i])){
				valueScore = i+1;
			}
		}
		for(int i =0; i < suits.length; i++){
			if(this.suit.equals(suits[i])){
				suitScore = (counter)/10;
			}
			counter++; //the first run of the loop counter will equal one and 1/10 will equal the value of spades
		}
		return valueScore + suitScore; //returns the sum of the score of the value and suit ex. 1.0 + 0.1
	}
}