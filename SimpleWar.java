public class SimpleWar{
	public static void main(String args[]){
		Deck warDeck = new Deck();
		warDeck.shuffle();
		
		//int variables for points and the round number
		int playerOnePoints = 0;
		int playerTwoPoints = 0;
		int roundNumber = 1;
		
		while(warDeck.length() > 0){ //this loop will run until there are no cards in the card
			System.out.println("\n" + "ROUND: " + roundNumber + "\n");
			
			//draws the two top cards in the War deck
			Card playerOneCard = warDeck.drawTopCard();
			Card playerTwoCard = warDeck.drawTopCard();
			
			System.out.println("Score: " + "\n" + "Player One: " + playerOnePoints + "\n" + "Player Two: " + playerTwoPoints + "\n"); //Shows the current score of the game
			
			System.out.println(playerOneCard); //prints the cards drawn by the player
			System.out.println(playerTwoCard);
			
			System.out.println(playerOneCard.calculateScore()); //prints the score of the card drawn
			System.out.println(playerTwoCard.calculateScore());
			
			
			if(playerOneCard.calculateScore() > playerTwoCard.calculateScore()){ //depending on whos score is higher, the print statement will say the player who won and increase the score of the game 
				System.out.println("Player One Wins this round" + "\n");
				playerOnePoints++;
			}
			else{
				System.out.println("Player Two Wins this round" + "\n");
				playerTwoPoints++;
			}
			System.out.println("Score: " + "\n" + "Player One: " + playerOnePoints + "\n" + "Player Two: " + playerTwoPoints + "\n");
			roundNumber++;
		}
		
		//print statements that show who wins the whole game or if the game is a draw by comparing the final scores
		if(playerOnePoints > playerTwoPoints){
			System.out.println("Congratulations Player 1. You win!!");
		}
		if(playerOnePoints < playerTwoPoints){
			System.out.println("Congratulations Player 2. You win!!");
		}
		if(playerOnePoints == playerTwoPoints){
			System.out.println("The game ends in a draw");
		}
	}
}