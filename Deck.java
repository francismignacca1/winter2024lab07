import java.util.Random;
public class Deck{
	private Card[] cards;
	private int numberOfCards;
	private Random rng;
	
	public Deck(){ //constructor
		this.numberOfCards = 52;
		this.cards = new Card[52];
		
		String[] suits = new String[]{"Hearts", "Spades", "Diamonds", "Clubs"};
		String[] values = new String[]{"Ace", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King"};
		
		int cardPosition = 0;
		for(int i = 0; i < suits.length; i++){
			for(int j = 0; j < values.length; j++){
				this.cards[cardPosition] = new Card(suits[i], values[j]);
				cardPosition++;
			}
		}
		Random rng = new Random();
		this.rng = rng;
	}

	public int length(){
		return this.numberOfCards;
	}
	public Card drawTopCard(){ //takes the last card in the deck which is the top card when a deck is faced down
		
		Card drawnCard = this.cards[this.numberOfCards-1];
		this.numberOfCards--;
		return drawnCard;
	}
	public String toString(){ //returns all the card objects in the deck and prints them each out on a new line 
		String cardList = "";
		for(int i = 0; i < this.cards.length; i++){
			cardList += this.cards[i] + "\n";
		}
		return cardList;
	}
	public void shuffle(){ //shuffles the deck and puts all of the cards in a random position in the deck
		Card placeHolderCard = this.cards[0];
		for(int i =0; i < this.cards.length-1; i++){
			int randomNumber = this.rng.nextInt(this.numberOfCards);
			placeHolderCard = this.cards[i];
			cards[i] = cards[randomNumber];
			cards[randomNumber] = placeHolderCard;
		}
	
	}
}